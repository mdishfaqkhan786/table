import {createStore,applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from 'redux-thunk'
import appReducer from "../reducer/reducer";
// import logger from "redux-logger"

const middleware = applyMiddleware(thunk)
const store = createStore(appReducer,middleware);
export default store;

