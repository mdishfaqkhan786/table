import React from 'react';
import logo from './logo.svg';
import {Provider} from "react-redux"
import './App.css';
import Infopage from './componets/Infopage'
import { Flex } from '@chakra-ui/layout';
import Menu_bar from "./componets/Menu_bar";
import store from "./redux/store/store"

function App() {
  return (
    <Provider store={store} >
      <Flex >
        <Menu_bar />
       <Infopage />

      </Flex>
    </Provider>
    
  );
}

export default App;
