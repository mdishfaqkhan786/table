import { Avatar } from '@chakra-ui/avatar';
import Icon from '@chakra-ui/icon';
import { Box,Flex,Text, Heading, Spacer, Link, } from '@chakra-ui/layout';
import React, { Component } from 'react';
import { IconBase } from 'react-icons';
import Tablecomp from "./Tablecomp";
import {FaSearch,FaBell} from 'react-icons/fa'
import { IconButton } from '@chakra-ui/button';


const infopagecomp:React.FC =()=>{
        return (
            <Flex width="100%" flexDirection="column" p=
            '20px' background="#f6f6f6">
                <Flex m="10px 5px" height="60px">
                    <Heading fontSize="26px" p='5px 5px'>Service</Heading>
                    <Spacer />
                    <Flex  p='5px 5px'>
                       
                    {/* <IconButton aria-label="Notice" m="0 10px" icon={<FaBell />} color="#C5C7CD" /> */}
                    <Link><Icon  m="0 10px" as={FaSearch} color="#C5C7CD" /></Link>
                    <Link><Icon  m="0 10px" as={FaBell} color="#C5C7CD" /></Link>
                    </Flex>
                    
                    <Text p="5px 5px" justifyItems="center" fontSize='16px' fontWeight="600">Mohd Ishfaq</Text>
                    
                    <Avatar size="sm"></Avatar>

                </Flex>
                <Tablecomp />

            </Flex>
            
        )
    }

export const Infopage= React.memo(infopagecomp)
export default Infopage
