import { Link, List, ListIcon, ListItem } from '@chakra-ui/react';
import React, { Component } from 'react';
import { MdSettings,MdPermContactCalendar ,MdPerson,MdPeopleOutline} from "react-icons/md";
import { FaLightbulb ,FaTicketAlt} from "react-icons/fa";
import { IoIosPaper} from "react-icons/io";

export class List_item extends Component {
    render() {
        var list =[
            
            {
                itemName: "Overview",
                itemLink:"./",
                icon: MdPeopleOutline
            },
            {
                itemName: "Tickets",
                itemLink:"./",
                icon:FaTicketAlt
            },
            {
                itemName: "Idea",
                itemLink:"",
                icon:FaLightbulb
            },
            {
                itemName: "contact",
                itemLink:"",
                icon:MdPermContactCalendar
            },{
                itemName: "Agents",
                itemLink:"",
                icon:MdPerson
            },
            {
                itemName: "Articles",
                itemLink:"",
                icon:IoIosPaper
            }

        
        ];
        const item = list.map((item:any) =>{
            // console.log(item.icon);
            return(<ListItem  w="255px" color= "#A4A6B3" _hover={{
                color:"#DDE2FF",
                background:" #9fa2b469" ,
                }} h="56px"  justifySelf="center" p="15px 50px">
                    <Link >
                    <ListIcon  as={item.icon} color="green.500" />
                 {item.itemName}
                    </Link>
                
            </ListItem>)})
        return (
            <List  m="0">
                {item}
            </List>
            
        )
    }
}

export default List_item
