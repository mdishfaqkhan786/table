import React, { Component,useEffect,useState } from 'react';
import {connect} from 'react-redux';

import {FaSortAmountUpAlt,FaFilter} from 'react-icons/fa'
import avt from '../image/avt.jpg'
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    Box,
    Flex,
    Text,
    Avatar,
    Spacer,
    Icon,
  } from "@chakra-ui/react";
import store from '../redux/store/store';
import { fatchData } from '../redux/action.ts/action';
  type Category = {
    id: string;
    name: string;
    description: string;
    tagLine: string | null;
    tags: string;
    icon: string;
    image: string;
    createdAt: string;
    updatedAt: string;
  };
  type Props = {
    items: Category[];
  };



const Tablecomp =()=>{

    const [items, setItems] = useState<any[]>([]);
    useEffect(() => {
        
             store.dispatch(fatchData());
             
             store.subscribe(()=>setItems(store.getState().user));
             console.log("table",store.getState());
             
    
        
                    }, []);
                   
        return (
           
            <Box  background="#FFFFFF" border="2px"  borderColor="gray.200" borderRadius="10px">
                 
                 <Flex p="10px" w='100%'>
                <Text >Service</Text>
                <Spacer />
                <Flex><Box m='0 20px'>
                    <Icon m='0 10px' as={FaSortAmountUpAlt} />Sort</Box>
                <Box m='2px'>
                <Icon as={FaFilter} m='0 10px' />Sort
                    Filter</Box></Flex>
                
                    
                    </Flex>
                <Table>
                
                <Thead>
               
                    
                    <Tr>
                    
                        <Th>Service Categories</Th>
                        <Th>Tags</Th>
                        <Th>Date</Th>
                        <Th>priority</Th>
                    </Tr>
                </Thead>
                <Tbody>
                
                
                       {items.map((item:any) =>{return<Tr>
                        <Td>
                            <Flex m="1px">
                                <Avatar src={item.icon} />
                            <Flex direction="column" ml="10px" > 
                                
                                <Text>{item.name}</Text>
                                <Text opacity="50%" fontSize="0.7em">{item.description}</Text>
                            </Flex></Flex>
                            
                            
                        </Td>
                        
                        <Td opacity="100%" fontSize="0.8em">{item.tags}</Td>
                        <Td>
                        <Flex direction="column"  >  
                                
                                <Text opacity="50%" fontSize="0.7em">{item.updatedAt} </Text>
                            </Flex>
                        </Td>
                        <Td>{item.priority}</Td>
                    </Tr>})}
                      
                </Tbody>
                </Table>
            </Box>
            
        )
    }

   
export default  Tablecomp
