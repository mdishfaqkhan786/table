import { Box, Divider, List, ListItem ,ListIcon,Icon} from '@chakra-ui/react';
import { MdSettings,MdPermContactCalendar ,MdPerson} from "react-icons/md";
import { FaLightbulb ,FaTicketAlt} from "react-icons/fa";
import { IoIosPaper} from "react-icons/io";
import List_item from './List_item'

import React, { Component } from 'react';


export class Menu_bar extends Component {
    
    render() {
      
                    
        return (
            
            <Box backgroundColor="#363740" w="255px" h="100vh">
                
                     <List_item />
                
                <Divider width="100%" />
                
                <List  w="150px" color= "#A4A6B3"  m="auto">
                    <ListItem h="56px"  p="10px 0">
                        <ListIcon as={MdSettings} color="green.500" />
                        Setting
                    </ListItem>
                </List>

            
            </Box>
            
        )
    }
}

export default Menu_bar
